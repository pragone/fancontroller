#include <stdio.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <math.h>

float getTemp();

// We'll use PIN 1, which is GPIO18
#define PIN 1
#define DEBUG 1
#define STARTTEMP  40.0f
#define IDLETEMP 37.0f
#define ENDTEMP 50.0f
#define IDLESPEED 256+128

int main (void)   {
	int speed = 0;
	int lastSpeed = 0;
	int middleSpeed = 0;
	float temp;
	if (wiringPiSetup () == -1) {
		exit (1) ;
	}

	// set pin 1 to PWM
	pinMode (1, PWM_OUTPUT) ;
	printf("Starting\n");
	while (1) {
		temp = getTemp();
		if (temp < IDLETEMP) {
  			speed = 0;
		} else if (temp < STARTTEMP) {
  			speed = IDLESPEED;
		} else if (temp >= ENDTEMP) {
  			speed = 1023;
		} else {
			speed = round((1023 - IDLESPEED) * (temp - STARTTEMP)/(ENDTEMP-STARTTEMP)) + IDLESPEED; 
		}
		if (speed > 1023) {
			speed = 1023;
		}
		if (DEBUG) {
			printf("CPU temperature is %f degrees C\n",temp);
		}
		middleSpeed = round((lastSpeed + speed)/2);
		if (lastSpeed != middleSpeed) {
			if (DEBUG) {
				printf("Calculated speed: %d, Last speed: %d, Setting speed to %d\n", speed, lastSpeed, middleSpeed);
			}
			lastSpeed = middleSpeed;
  			pwmWrite (PIN, lastSpeed) ;
		}
		delay (5000);
	}
  return 0 ;
}
float getTemp() {
	float millideg;
	FILE *thermal;
	int n;
	
	thermal = fopen("/sys/class/thermal/thermal_zone0/temp","r");
	n = fscanf(thermal,"%f",&millideg);
	fclose(thermal);
	return millideg / 1000;
}
