#!/bin/bash

cd $(dirname $0) > /dev/null

if [ ! -d WiringPi ]; then
	echo "Installing WiringPi. It'll ask you for your password to sudo"
	git clone https://github.com/WiringPi/WiringPi
	cd WiringPi
	./build
	cd ..
fi

echo "Building fancontroller"
gcc -o fancontroller -lwiringPi -lm fancontroller.c

echo "Installing"
if sudo systemctl status fancontroller.service >/dev/null 2>&1; then
	sudo systemctl stop fancontroller.service
fi
sudo cp fancontroller /usr/bin/
sudo cp fancontroller.service /etc/systemd/system/fancontroller.service
sudo systemctl daemon-reload
sudo systemctl start fancontroller.service
if [ "$(sudo systemctl is-enabled fancontroller.service)" != "enabled" ]; then
	sudo systemctl enable fancontroller.service
fi
